﻿using System;
using System.Collections.Generic;

namespace Bai_KT.Models;

public partial class Customer
{
    public int CustomerId { get; set; }

    public string? Firstname { get; set; }

    public string? LastName { get; set; }

    public string? ContactAndAddress { get; set; }

    public virtual ICollection<Account> Accounts { get; set; } = new List<Account>();

    public virtual ICollection<Transaction> Transactions { get; set; } = new List<Transaction>();
}
