﻿using System;
using System.Collections.Generic;

namespace Bai_KT.Models;

public partial class Employee
{
    public int EmployeeId { get; set; }

    public string? Fisrtname { get; set; }

    public string? Lastname { get; set; }

    public string? ContactAndAddress { get; set; }

    public virtual ICollection<Transaction> Transactions { get; set; } = new List<Transaction>();
}
