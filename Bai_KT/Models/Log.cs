﻿using System;
using System.Collections.Generic;

namespace Bai_KT.Models;

public partial class Log
{
    public int LogsId { get; set; }

    public int? TransactionId { get; set; }

    public DateOnly? Logindate { get; set; }

    public TimeOnly? Logtintime { get; set; }

    public virtual ICollection<Report> Reports { get; set; } = new List<Report>();
}
