﻿using System;
using System.Collections.Generic;

namespace Bai_KT.Models;

public partial class Transaction
{
    public int TransactionalId { get; set; }

    public int? EmployeeId { get; set; }

    public int? CustomerId { get; set; }

    public string? Name { get; set; }

    public virtual Customer? Employee { get; set; }

    public virtual Employee? EmployeeNavigation { get; set; }

    public virtual ICollection<Report> Reports { get; set; } = new List<Report>();
}
